# Common Gateway Model for Converting Pharmaceutical Forms (Web Interface)

XML is used as a common gateway for file conversion between pharmacies.
This system uses a universal template such that all pharmacies can retain their own original forms while being able to read the forms sent to them by other pharmacies.
After files are converted, they are verified before being sent.

## Demo
+ Open [index.html](http://htmlpreview.github.io/?https://bitbucket.org/aya43/commongatewaypharmaceutical/src/fb3c1c8575ede8f5d926ef0b2765830e0eef9a3c/demo/index.html?at=master) and follow the navigation in the top menu.
+ Try out the XML conversions using samples files in [XML](./XML).
+ Rules used tot verify the XMLs are in [XSD](./XSD).
+ [db_interaction.png](./db_interaction.png) is the interaction diagram that describes the human computer interaction between the user and the web interface
+ [ERD.png](./ERD.png) is the ERD (Entity Relationship Diagram) that displays the design of the database.

![screenshot.png](./screenshot.png)